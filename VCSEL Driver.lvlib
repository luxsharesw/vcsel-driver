﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="Class" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Get Driver Data" Type="Folder">
			<Item Name="Get Driver Data.lvclass" Type="LVClass" URL="../Class/Get Driver Data/Get Driver Data.lvclass"/>
		</Item>
		<Item Name="Set Bias" Type="Folder">
			<Item Name="Set Bias.lvclass" Type="LVClass" URL="../Class/Set Bias/Set Bias.lvclass"/>
		</Item>
		<Item Name="Set Modulation" Type="Folder">
			<Item Name="Set Modulation.lvclass" Type="LVClass" URL="../Class/Set Modulation/Set Modulation.lvclass"/>
		</Item>
		<Item Name="Set DC Gain" Type="Folder">
			<Item Name="Set DC Gain.lvclass" Type="LVClass" URL="../Class/Set DC Gain/Set DC Gain.lvclass"/>
		</Item>
		<Item Name="Set AC Gain" Type="Folder">
			<Item Name="Set AC Gain.lvclass" Type="LVClass" URL="../Class/Set AC Gain/Set AC Gain.lvclass"/>
		</Item>
		<Item Name="Set Rvcsel" Type="Folder">
			<Item Name="Set Rvcsel.lvclass" Type="LVClass" URL="../Class/Set Rvcsel/Set Rvcsel.lvclass"/>
		</Item>
		<Item Name="Set Aute Mute" Type="Folder">
			<Item Name="Set Auto Mute.lvclass" Type="LVClass" URL="../Class/Set Auto Mute/Set Auto Mute.lvclass"/>
		</Item>
		<Item Name="Set Burn-in" Type="Folder">
			<Item Name="Set Burn-in.lvclass" Type="LVClass" URL="../Class/Set Burn-in/Set Burn-in.lvclass"/>
		</Item>
		<Item Name="Set Burn-in Current" Type="Folder">
			<Item Name="Set Burn-in Current.lvclass" Type="LVClass" URL="../Class/Set Burn-in Current/Set Burn-in Current.lvclass"/>
		</Item>
		<Item Name="Set CDR Bypass" Type="Folder">
			<Item Name="Set CDR Bypass.lvclass" Type="LVClass" URL="../Class/Set CDR Bypass/Set CDR Bypass.lvclass"/>
		</Item>
		<Item Name="Set Power Down" Type="Folder">
			<Item Name="Set Power Down.lvclass" Type="LVClass" URL="../Class/Set Power Down/Set Power Down.lvclass"/>
		</Item>
		<Item Name="Set Tx Disable" Type="Folder">
			<Item Name="Set Tx Disable.lvclass" Type="LVClass" URL="../Class/Set Tx Disable/Set Tx Disable.lvclass"/>
		</Item>
		<Item Name="Get Bias" Type="Folder">
			<Item Name="Get Bias.lvclass" Type="LVClass" URL="../Class/Get Bias/Get Bias.lvclass"/>
		</Item>
	</Item>
	<Item Name="Public" Type="Folder">
		<Item Name="Get Product Device[].vi" Type="VI" URL="../Public/Get Product Device[].vi"/>
	</Item>
	<Item Name="VCSEL Driver.lvclass" Type="LVClass" URL="../VCSEL Driver.lvclass"/>
</Library>
